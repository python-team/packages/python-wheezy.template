python-wheezy.template (3.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop alternatives which existed for Python 2 co-installability.
  * Update the copyrights.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 31 Dec 2023 13:05:20 +0000

python-wheezy.template (0.1.167-4) unstable; urgency=medium

  * Fix sections for binary package python3-wheezy.template (utils ⇒ python)
  * Update standards version to 4.6.2, no changes needed.
  * Set Rules-Requires-Root: no.
  * Fix Uploaders.
  * Use dh-sequence-python3.
  * Add debian/watch file, using pypi.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 31 Dec 2023 12:24:31 +0000

python-wheezy.template (0.1.167-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Jun 2022 20:31:46 -0400

python-wheezy.template (0.1.167-2) unstable; urgency=medium

  * Team upload.
  * d/copyright: Use https protocol in Format field.
  * Use debhelper-compat instead of debian/compat.
  * d/control: Set Vcs-* to salsa.debian.org.
  * Drop Python 2 support.
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.
  * Bump standars version to 4.4.0 (no changes).
  * Enable all hardening.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 08:31:20 +0200

python-wheezy.template (0.1.167-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add prerm scripts to clean up the alternative.  (Closes: #842077)

 -- Andreas Beckmann <anbe@debian.org>  Wed, 18 Jan 2017 17:00:54 +0100

python-wheezy.template (0.1.167-1) unstable; urgency=medium

  * Initial release.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 14 Oct 2016 19:26:27 +0200
